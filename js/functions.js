const SERVER_URL = 'http://localhost:8080'



function getAll() {

    var headers = new Headers();
    var options = {
        method: 'GET',
        headers: headers,
        redirect: 'follow'
    };     

    var url = SERVER_URL + '/api/users/all'
    console.log('url: ' + url)
    fetch(url, options)
    .then(res => res.json())
    .then(out => {
        console.log(out)

        var tableRows = ''
        out.forEach((user) => {
            var html = `
                <th scope="row">${user.id}</th>
                <td>${user.lastName} ${user.firstName}</td>
                <td>${user.birthDate}</td>
                <td>${user.email}</td>
            `
            tableRows = tableRows + '<tr>' + html + '</tr>'
        })

        document.getElementById( 'tablePlaceholder' ).innerHTML = tableRows
    })
    .catch(error => {
        console.error('error', error)
        document.getElementById( 'errorPlaceholder' ).innerHTML = `
            Произошка ошибка в отправлении запроса на сервер.
            <br>Техническое описание проблемы:
            <pre>${error}</pre>
        `
    });                                                
}





function getById(id) {
    var headers = new Headers();
    var options = {
        method: 'GET',
        headers: headers,
        redirect: 'follow'
    };     
    if ( ! id ) [
        id = document.getElementById('userId').value
    ]
    document.getElementById( 'userDetailPlaceholder' ).innerHTML = ''
    var url = SERVER_URL + '/api/users/' + id
    console.log('url: ' + url)
    fetch(url, options)
    .then(async (res) => {
        if ( res.ok ) {
            var user = await res.json()
            let html = `
            <h3>${user.lastName} ${user.firstName}</h3>
            <div class="grid">
            <p><strong>${user.email}</strong></p>
            <p><em>${user.login}</em></p>
            <p><u>${user.birthDate}</u></p>
            </div>
            `
            document.getElementById( 'userDetailPlaceholder' ).innerHTML = html
        }
        else {
            if ( res.status === 404 ) {
                document.getElementById( 'errorPlaceholder' ).innerHTML = `В БД нет пользователя с ID = ${id}`    
            }
            else {
                document.getElementById( 'errorPlaceholder' ).innerHTML = `Произошла внутренняя ошибка сервера (код ошибки: ${res.status}).`
            }
        }
    })
    .catch(error => {
        console.error('error', error)
        document.getElementById( 'errorPlaceholder' ).innerHTML = `
            Произошка ошибка в отправлении запроса на сервер.
            <br>Техническое описание проблемы:
            <pre>${error}</pre>
        `
    });  
}

      